import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService{
  createDb() {
    const heroes = [
      { id: 11, name: 'Squall',  skill: 'Renzokuken', weapons:
          [{name: 'Revolver', type: 0, equipped: true},
           {name: 'Cutting Trigger', type: 0, equipped: false }]
      },
      { id: 12, name: 'Zell',    skill: 'Duel',       weapons:
          [{name: 'Metal Knuckle', type: 0, equipped: true}]
      },
      { id: 13, name: 'Irvine',  skill: 'Shoot',      weapons: [{name: 'Two', type: 0, equipped: false}]    },
      { id: 14, name: 'Selphie', skill: 'Slot',       weapons: [{name: 'Three', type: 0, equipped: false}]    },
      { id: 15, name: 'Rinoa',   skill: 'Angel Wing', weapons: [{name: 'Four', type: 0, equipped: false}]    },
      { id: 16, name: 'Quistis', skill: 'Blue Magic', weapons: [{name: 'Five', type: 0, equipped: false}]    },
      { id: 17, name: 'Tidus',   skill: 'Swordplay',  weapons: [{name: 'Six', type: 0, equipped: false}]    },
      { id: 18, name: 'Yuna',    skill: 'Grand Summon', weapons: [{name: 'Seven', type: 0, equipped: false}]  },
      { id: 19, name: 'Wakka',   skill: 'Reels',        weapons: [{name: 'Eight', type: 1, equipped: false}]  },
      { id: 20, name: 'Lulu',    skill: 'Fury',         weapons: [{name: 'Mog', type: 2, equipped: false}]  }
    ];
    return {heroes};
  }
}
