import { Weapon } from './weapon';
export class Hero {
  id: number;
  name: string;
  skill: string;
  weapons: Weapon[];

  /**
  get Equipped(): string {
    const weapon = this.weapons.find(w => w.equipped === true);
    return ( weapon === undefined ? 'none' : weapon.name );
  }
 */

}
