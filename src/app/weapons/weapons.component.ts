import {Component, Input, OnInit} from '@angular/core';
import { Weapon } from '../weapon';
import {Hero} from '../hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-weapons',
  templateUrl: './weapons.component.html',
  styleUrls: ['./weapons.component.css']
})
export class WeaponsComponent implements OnInit {

  @Input() hero: Hero;
  constructor(private heroService: HeroService) { }

  ngOnInit() {
  }

  colorEquipped(weapon: Weapon): string {
    return weapon.equipped === true ? '#353535' : '#EEE';
  }

  colorBadge(type: number): string {
    return 'var(--weapon-' + type + ')';
  }

  add(name: string, type: number, equipped: boolean): void {
    name = name.trim();
    if (!name) { return; }
    const weapon = {name: name, type: type, equipped: equipped} as Weapon;
    this.hero.weapons.push(weapon);
    this.heroService.updateHero(this.hero);
    if (equipped === true) { this.equip(weapon); }
  }

  delete(weapon: Weapon): void {
    this.hero.weapons = this.hero.weapons.filter(w => w !== weapon);
    this.heroService.updateHero(this.hero);
  }

  equip(weapon: Weapon): void {
    this.hero.weapons.forEach(w => this.unequip(w));
    this.hero.weapons.find(w => w === weapon).equipped = true;
  }

  unequip(item): void {
    item.equipped = false;
  }

  isEquipped(weapon: Weapon): string {
    if (!weapon.equipped) {
      return 'Equip';
    }
    return 'Using';
  }
}
