export enum WeaponType {
  Melee,
  Range,
  Magical,
}
