import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive ({
  selector: '[appHeroHighlight]'
})
export class HeroHighlightDirective implements OnInit {
  constructor(private element: ElementRef) {}

  ngOnInit() {
    this.element.nativeElement.style.backgroundColor = 'green';
  }

}
