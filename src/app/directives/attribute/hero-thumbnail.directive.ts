import {Directive, HostBinding, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appHeroThumbnail]'
})
export class HeroThumbnailDirective {
  @Input('appHeroThumbnail') index: number;
  showing: boolean;
  constructor() { }

  @HostListener('mouseenter') mousein() {
    this.showing = true;
  }
}
