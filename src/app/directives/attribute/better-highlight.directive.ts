 import {Directive, ElementRef, OnInit, Renderer2, HostListener, HostBinding, Input} from '@angular/core';



@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {
  @Input() defaultColor = 'transparent';
  @Input() highlightColor = 'blue';
  @Input() textDefaultColor = 'black';
  @Input() textHighlightColor = 'white';
  @HostBinding('style.backgroundColor') backgroundColor: string;
  @HostBinding('style.color') textColor: string;

  constructor() {}
  // constructor(private element: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.backgroundColor = this.defaultColor;
    this.textColor = this.textDefaultColor;
    // this.renderer.setStyle(this.element.nativeElement, 'background-color', '#607D8B');
    // like this it will always set the background color
  }

  @HostListener('mouseenter') mousein() {
    // this.renderer.setStyle(this.element.nativeElement, 'background-color', 'blue');
    this.backgroundColor = this.highlightColor;
    this.textColor = this.textHighlightColor;
  }

  @HostListener('mouseleave') mouseout() {
    // this.renderer.setStyle(this.element.nativeElement, 'background-color', '#607D8B');
    this.backgroundColor = this.defaultColor;
    this.textColor = this.textDefaultColor;
  }
}
