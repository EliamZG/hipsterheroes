import {WeaponType} from './enums/weapon-type.enum';

export class Weapon{
  name: string;
  type: WeaponType;
  equipped: boolean;
}
