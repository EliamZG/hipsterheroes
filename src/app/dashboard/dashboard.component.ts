import {Component, Input, OnInit} from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
  @Input() thumbnails: boolean[] = [false, false, false, false, false];

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes.slice(0, 4));
  }

  getEquippedWeapon(hero: Hero): string {
    const weapon = hero.weapons.find(w => w.equipped === true);
    return ( weapon === undefined ? 'none' : weapon.name );
  }

  toggle(i: number): void {
    this.thumbnails[i] = !this.thumbnails[i];
  }
}
